
import java.util.Arrays;
import java.util.stream.Collectors;

public class StripComments {

	public static String stripComments(String text, String[] commentSymbols) {
    
    String commentDelims = "[" + String.join("", commentSymbols) + "]+";
    return Arrays.stream(text.split("\\n"))
            .map(line -> line.split(commentDelims).length > 0 ? line.split(commentDelims)[0].stripTrailing() : "")
            .collect(Collectors.joining("\n"));
	}
	
}